__author__ = 'Jure Bevc'

from random import randint, random
from time import time, sleep
from datetime import datetime
from tkinter import *
from math import floor


# move naredi premik, ugotovi, ce je nonant po novem zaseden in doloci, kam je mozno igrati naslednjo potezo.
def move(game_board,
         available_fields,
         first_player,
         is_field_finished,
         winners_big_fields,
         big_field_coordinate,
         small_field_coordinate):
    if available_fields[big_field_coordinate]:
        previously_available_fields = available_fields
        if game_board[big_field_coordinate][small_field_coordinate] is None:
            if first_player:
                game_board[big_field_coordinate][small_field_coordinate] = True
            else:
                game_board[big_field_coordinate][small_field_coordinate] = False
            [is_field_finished, winners_big_fields] = big_field_over(game_board, is_field_finished, winners_big_fields,
                                                                     big_field_coordinate)
            available_fields = determine_available_fields(is_field_finished, small_field_coordinate)
            return [game_board, available_fields, is_field_finished, winners_big_fields, not first_player,
                    previously_available_fields]
        else:
            raise Exception('Illegal move, wrong small field')
    else:
        raise Exception('Illegal move, wrong big field')


# undo_last_move povrne stanje na igralni plosci v tisto, ki je obstajalo pred zadnjo potezo.
def undo_last_move(game_board,
                   first_player,
                   is_field_finished,
                   winners_big_fields,
                   big_field_coordinate,
                   small_field_coordinate,
                   previously_available_fields):
    game_board[big_field_coordinate][small_field_coordinate] = None
    is_field_finished[big_field_coordinate] = False
    winners_big_fields[big_field_coordinate] = None
    return [game_board, is_field_finished, winners_big_fields, previously_available_fields, not first_player]


# big_field_over doloci, ali je nonant zakljucen in kdo je v njem zmagal.
def big_field_over(game_board,
                   is_field_finished,
                   winners_big_fields,
                   big_field_coordinate):
    if is_field_finished[big_field_coordinate]:
        return [is_field_finished, winners_big_fields]
    rows = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)]
    for row in rows:
        if game_board[big_field_coordinate][row[0]] is not None and game_board[big_field_coordinate][row[0]] == \
                game_board[big_field_coordinate][row[1]] == game_board[big_field_coordinate][row[2]]:
            is_field_finished[big_field_coordinate] = True
            winners_big_fields[big_field_coordinate] = game_board[big_field_coordinate][row[0]]
            return [is_field_finished, winners_big_fields]
    for small_field in game_board[big_field_coordinate]:
        if small_field is None:
            return [is_field_finished, winners_big_fields]
    is_field_finished[big_field_coordinate] = True
    return [is_field_finished, winners_big_fields]


# determine_available_fields doloci, kam lahko odigramo naslednjo potezo.
def determine_available_fields(is_field_finished, last_move_small_field_coordinate):
    available_fields = [False for _ in range(9)]
    #
    if is_field_finished[last_move_small_field_coordinate]:
        for big_field in range(9):
            if not is_field_finished[big_field]:
                available_fields[big_field] = True
    elif not is_field_finished[last_move_small_field_coordinate]:
        available_fields[last_move_small_field_coordinate] = True
    return available_fields


# game_over ugotovi, ali je igre konec in kdo je zmagovalec.
def game_over(is_field_finished, winners_big_fields, available_fields):
    rows = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)]
    for row in rows:
        if is_field_finished[row[0]] and winners_big_fields[row[0]] is not None and \
                winners_big_fields[row[0]] == winners_big_fields[row[1]] == winners_big_fields[row[2]]:
            return [winners_big_fields[row[0]], True]
    # ce ni na voljo nobene poteze, je igre konec
    for field in available_fields:
        if field:
            return [None, False]
    return [None, True]


def human_player_move(human_player_is_first_player,
                      game_board,
                      available_fields):
    if human_player_is_first_player:
        print('First player, it\'s your turn!')
        while True:
            try:
                move1 = int(input('Enter big_field_coordinate: '))
                break
            except ValueError:
                print('big_field_coordinate should be an integer between 1 and 9.')
        while 1 > move1 or move1 > 10 or not available_fields[move1 - 1]:
            print('big_field_coordinate should be an integer between 1 and 9, and should represent a legal field.')
            while True:
                try:
                    move1 = int(input('Enter big_field_coordinate: '))
                    break
                except ValueError:
                    print('big_field_coordinate should be an integer between 1 and 9.')
        while True:
            try:
                move2 = int(input('Enter small_field_coordinate: '))
                break
            except ValueError:
                print('small_field_coordinate should be an integer between 1 and 9.')

        while 1 > move2 or move2 > 10 or game_board[move1 - 1][move2 - 1] is not None:
            print('small_field_coordinate should be an integer between 1 and 9, and should represent a legal field.')
            while True:
                try:
                    move2 = int(input('Enter small_field_coordinate: '))
                    break
                except ValueError:
                    print('small_field_coordinate should be an integer between 1 and 9.')
        human_move = (move1 - 1, move2 - 1)
        return ['human_input', human_move]

    else:
        print('Second player, it\'s your turn!')
        while True:
            try:
                move1 = int(input('Enter big_field_coordinate: '))
                break
            except ValueError:
                print('big_field_coordinate should be an integer between 1 and 9.')
        while 1 > move1 or move1 > 10 or not available_fields[move1 - 1]:
            print('big_field_coordinate should be an integer between 1 and 9, and should represent a legal field.')
            while True:
                try:
                    move1 = int(input('Enter big_field_coordinate: '))
                    break
                except ValueError:
                    print('big_field_coordinate should be an integer between 1 and 9.')
        while True:
            try:
                move2 = int(input('Enter small_field_coordinate: '))
                break
            except ValueError:
                print('small_field_coordinate should be an integer between 1 and 9.')

        while 1 > move2 or move2 > 10 or game_board[move1 - 1][move2 - 1] is not None:
            print('small_field_coordinate should be an integer between 1 and 9, and should represent a legal field.')
            while True:
                try:
                    move2 = int(input('Enter small_field_coordinate: '))
                    break
                except ValueError:
                    print('small_field_coordinate should be an integer between 1 and 9.')
        human_move = (move1 - 1, move2 - 1)
        return ['human_input', human_move]


# alpha_beta pruning. Premikamo se po drevesu igre do zeljene globine, tam izracunamo hevristicno ovrednotenje, nato
# izvajamo alpha-beta pruning.
def alpha_beta_pruning(max_depth,
                       depth,
                       alpha, beta,
                       maximizing_player,
                       actual_first_player,
                       game_board,
                       first_player,
                       available_fields,
                       is_field_finished,
                       winners_big_fields,
                       value_small_single_me=10,
                       value_small_single_opponent=10,
                       value_small_double_me=20,
                       value_small_double_opponent=20,
                       value_big_single_me=100,
                       value_big_single_opponent=100,
                       value_big_double_me=200,
                       value_big_double_opponent=200,
                       value_moves_me=30,
                       value_moves_opponent=30):
    # print(alpha, beta)
    # ce smo na maksimalni globini ali je igre konec, izracunamo hevristicno ovrednotenje.
    current_best_move = 'not_a_move'
    if depth == 0 or game_over(is_field_finished, winners_big_fields, available_fields)[1]:
        if actual_first_player:
            return [heuristic_evaluation2(actual_first_player,
                                          game_board,
                                          is_field_finished,
                                          winners_big_fields,
                                          available_fields,
                                          first_player,
                                          depth,
                                          value_small_single_me,
                                          value_small_single_opponent,
                                          value_small_double_me,
                                          value_small_double_opponent,
                                          value_big_single_me,
                                          value_big_single_opponent,
                                          value_big_double_me,
                                          value_big_double_opponent,
                                          value_moves_me,
                                          value_moves_opponent), current_best_move]
        else:
            return [heuristic_evaluation2(actual_first_player,
                                          game_board,
                                          is_field_finished,
                                          winners_big_fields,
                                          available_fields,
                                          first_player,
                                          depth,
                                          value_small_single_me,
                                          value_small_single_opponent,
                                          value_small_double_me,
                                          value_small_double_opponent,
                                          value_big_single_me,
                                          value_big_single_opponent,
                                          value_big_double_me,
                                          value_big_double_opponent,
                                          value_moves_me,
                                          value_moves_opponent), current_best_move]
    # alpha:
    elif maximizing_player:
        v = float('-inf')
        for big_field in range(9):
            if available_fields[big_field]:
                for small_field in range(9):
                    if game_board[big_field][small_field] is None:
                        # shranimo trenutno situacijo
                        # izvedemo potezo, izvedemo alpha-beta
                        [new_game_board, new_available_fields, new_is_field_finished, new_winners_big_fields,
                         new_first_player, previously_available_fields] = \
                            move(game_board, available_fields, first_player, is_field_finished, winners_big_fields,
                                 big_field, small_field)
                        alpha_beta = alpha_beta_pruning(max_depth,
                                                        depth - 1,
                                                        alpha,
                                                        beta,
                                                        not maximizing_player,
                                                        actual_first_player,
                                                        new_game_board,
                                                        new_first_player,
                                                        new_available_fields,
                                                        new_is_field_finished,
                                                        new_winners_big_fields,
                                                        value_small_single_me,
                                                        value_small_single_opponent,
                                                        value_small_double_me,
                                                        value_small_double_opponent,
                                                        value_big_single_me,
                                                        value_big_single_opponent,
                                                        value_big_double_me,
                                                        value_big_double_opponent,
                                                        value_moves_me,
                                                        value_moves_opponent)
                        if v < max(v, alpha_beta[0]):
                            if depth == max_depth:
                                current_best_move = (big_field, small_field)
                            v = max(v, alpha_beta[0])
                        alpha = max(alpha, v)
                        # preidemo nazaj na prejsnjo situacijo
                        undo_last_move(game_board, first_player, is_field_finished, winners_big_fields, big_field,
                                       small_field, previously_available_fields)
                        if beta <= alpha:
                            break
                if beta <= alpha:
                    break
        return v, current_best_move
    # beta:
    else:
        v = float('inf')
        for big_field in range(9):
            if available_fields[big_field]:
                for small_field in range(9):
                    if game_board[big_field][small_field] is None:
                        # izvedemo potezo, izvedemo alpha-beta
                        [new_game_board, new_available_fields, new_is_field_finished, new_winners_big_fields,
                         new_first_player, previously_available_fields] = \
                            move(game_board, available_fields, first_player, is_field_finished, winners_big_fields,
                                 big_field, small_field)
                        alpha_beta = alpha_beta_pruning(max_depth,
                                                        depth - 1,
                                                        alpha,
                                                        beta,
                                                        not maximizing_player,
                                                        actual_first_player,
                                                        new_game_board,
                                                        new_first_player,
                                                        new_available_fields,
                                                        new_is_field_finished,
                                                        new_winners_big_fields,
                                                        value_small_single_me,
                                                        value_small_single_opponent,
                                                        value_small_double_me,
                                                        value_small_double_opponent,
                                                        value_big_single_me,
                                                        value_big_single_opponent,
                                                        value_big_double_me,
                                                        value_big_double_opponent,
                                                        value_moves_me,
                                                        value_moves_opponent)
                        if v > min(v, alpha_beta[0]):
                            if depth == max_depth:
                                current_best_move = (big_field, small_field)
                            v = min(v, alpha_beta[0])
                        beta = min(beta, v)
                        # preidemo nazaj na prejsnjo situacijo
                        undo_last_move(game_board, first_player, is_field_finished, winners_big_fields, big_field,
                                       small_field, previously_available_fields)
                        if beta <= alpha:
                            break
                if beta <= alpha:
                    break
        return v, current_best_move


# trenutno se hef1 ne uporablja.
def heuristic_evaluation1(actual_first_player,
                          game_board,
                          is_field_finished,
                          winners_big_fields,
                          available_fields,
                          first_player,
                          depth,
                          value_big_central_field_me=100,
                          value_big_central_field_opponent=100,
                          value_big_corner_fields_me=80,
                          value_big_corner_fields_opponent=80,
                          value_big_border_fields_me=70,
                          value_big_border_fields_opponent=70,
                          value_small_central_fields_me=10,
                          value_small_central_fields_opponent=10,
                          value_small_corner_fields_me=8,
                          value_small_corner_fields_opponent=8,
                          value_small_border_fields_me=7,
                          value_small_border_fields_opponent=7,
                          value_moves_opponent=15):
    [winner, _] = game_over(is_field_finished, winners_big_fields, available_fields)
    if winner == actual_first_player:
        return 100000 + depth
    elif winner is not None:
        return -100000
    else:
        value = 0
        for big_field in range(9):
            if winners_big_fields[big_field] == actual_first_player:
                if big_field == 4:
                    value += value_big_central_field_me
                elif big_field == 0 or big_field == 2 or big_field == 6 or big_field == 8:
                    value += value_big_corner_fields_me
                else:
                    value += value_big_border_fields_me
            elif not is_field_finished[big_field]:
                if first_player == actual_first_player:
                    if available_fields[big_field]:
                        value += value_moves_opponent
                else:
                    if available_fields[big_field]:
                        value -= value_moves_opponent
                small_value = 0
                small_fields_full = 0
                for small_field in range(9):
                    if game_board[big_field][small_field] == actual_first_player:
                        if small_field == 4:
                            small_value += value_small_central_fields_me
                        elif small_field == 0 or small_field == 2 or small_field == 6 or small_field == 8:
                            small_value += value_small_corner_fields_me
                        else:
                            small_value += value_small_border_fields_me
                        small_fields_full += 1
                    elif game_board[big_field][small_field] is None:
                        pass
                    else:
                        if small_field == 4:
                            small_value += value_small_central_fields_opponent
                        elif small_field == 0 or small_field == 2 or small_field == 6 or small_field == 8:
                            small_value += value_small_corner_fields_opponent
                        else:
                            small_value += value_small_border_fields_opponent
                        small_fields_full += 1
                if small_fields_full == 0:
                    pass
                else:
                    value += small_value / small_fields_full
            elif winners_big_fields[big_field] is not actual_first_player:
                if big_field == 4:
                    value -= value_big_central_field_opponent
                elif big_field == 0 or big_field == 2 or big_field == 6 or big_field == 8:
                    value -= value_big_corner_fields_opponent
                else:
                    value -= value_big_border_fields_opponent
                    # dodamo se nakljucni int, da ne dobimo vedno istega rezultata
    return value  # + randint(1, 15)


def heuristic_evaluation2(actual_first_player,
                          game_board,
                          is_field_finished,
                          winners_big_fields,
                          available_fields,
                          first_player,
                          depth,
                          value_small_single_me=10,
                          value_small_single_opponent=10,
                          value_small_double_me=20,
                          value_small_double_opponent=20,
                          value_big_single_me=100,
                          value_big_single_opponent=100,
                          value_big_double_me=200,
                          value_big_double_opponent=200,
                          value_moves_me=30,
                          value_moves_opponent=30):
    [winner, _] = game_over(is_field_finished, winners_big_fields, available_fields)
    if winner == actual_first_player:
        return 100000 + depth
    elif winner is not None:
        return -100000
    else:
        rows = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)]
        value = 0
        # izracunaj vrednosti pozicije v mrezi velikih polj
        for big_field in range(9):
            # stevilo potez, ki so na voljo:
            if available_fields[big_field]:
                if actual_first_player == first_player:
                    value -= value_moves_opponent
                else:
                    value += value_moves_me
            if big_field == 0:
                for row_containing_big_field in [rows[0], rows[3], rows[6]]:
                    number_full_fields = 0
                    actual_first_player_present = False
                    actual_second_player_present = False
                    for big_field_in_row in row_containing_big_field:
                        if winners_big_fields[big_field_in_row] == actual_first_player:
                            actual_first_player_present = True
                            number_full_fields += 1
                        elif winners_big_fields[big_field_in_row] is not None:
                            actual_second_player_present = True
                            number_full_fields += 1
                    if actual_first_player_present and actual_second_player_present:
                        pass
                    elif actual_first_player_present:
                        if number_full_fields == 1:
                            value += value_big_single_me
                        elif number_full_fields == 2:
                            value += value_big_double_me
                    elif actual_second_player_present:
                        if number_full_fields == 1:
                            value -= value_big_single_opponent
                        elif number_full_fields == 2:
                            value -= value_big_double_opponent
            elif big_field == 1:
                for row_containing_big_field in [rows[4]]:
                    number_full_fields = 0
                    actual_first_player_present = False
                    actual_second_player_present = False
                    for big_field_in_row in row_containing_big_field:
                        if winners_big_fields[big_field_in_row] == actual_first_player:
                            actual_first_player_present = True
                            number_full_fields += 1
                        elif winners_big_fields[big_field_in_row] is not None:
                            actual_second_player_present = True
                            number_full_fields += 1
                    if actual_first_player_present and actual_second_player_present:
                        pass
                    elif actual_first_player_present:
                        if number_full_fields == 1:
                            value += value_big_single_me
                        elif number_full_fields == 2:
                            value += value_big_double_me
                    elif actual_second_player_present:
                        if number_full_fields == 1:
                            value -= value_big_single_opponent
                        elif number_full_fields == 2:
                            value -= value_big_double_opponent
            elif big_field == 2:
                for row_containing_big_field in [rows[5], rows[7]]:
                    number_full_fields = 0
                    actual_first_player_present = False
                    actual_second_player_present = False
                    for big_field_in_row in row_containing_big_field:
                        if winners_big_fields[big_field_in_row] == actual_first_player:
                            actual_first_player_present = True
                            number_full_fields += 1
                        elif winners_big_fields[big_field_in_row] is not None:
                            actual_second_player_present = True
                            number_full_fields += 1
                    if actual_first_player_present and actual_second_player_present:
                        pass
                    elif actual_first_player_present:
                        if number_full_fields == 1:
                            value += value_big_single_me
                        elif number_full_fields == 2:
                            value += value_big_double_me
                    elif actual_second_player_present:
                        if number_full_fields == 1:
                            value -= value_big_single_opponent
                        elif number_full_fields == 2:
                            value -= value_big_double_opponent
            elif big_field == 3:
                for row_containing_big_field in [rows[1]]:
                    number_full_fields = 0
                    actual_first_player_present = False
                    actual_second_player_present = False
                    for big_field_in_row in row_containing_big_field:
                        if winners_big_fields[big_field_in_row] == actual_first_player:
                            actual_first_player_present = True
                            number_full_fields += 1
                        elif winners_big_fields[big_field_in_row] is not None:
                            actual_second_player_present = True
                            number_full_fields += 1
                    if actual_first_player_present and actual_second_player_present:
                        pass
                    elif actual_first_player_present:
                        if number_full_fields == 1:
                            value += value_big_single_me
                        elif number_full_fields == 2:
                            value += value_big_double_me
                    elif actual_second_player_present:
                        if number_full_fields == 1:
                            value -= value_big_single_opponent
                        elif number_full_fields == 2:
                            value -= value_big_double_opponent
            elif big_field == 6:
                for row_containing_big_field in [rows[2]]:
                    number_full_fields = 0
                    actual_first_player_present = False
                    actual_second_player_present = False
                    for big_field_in_row in row_containing_big_field:
                        if winners_big_fields[big_field_in_row] == actual_first_player:
                            actual_first_player_present = True
                            number_full_fields += 1
                        elif winners_big_fields[big_field_in_row] is not None:
                            actual_second_player_present = True
                            number_full_fields += 1
                    if actual_first_player_present and actual_second_player_present:
                        pass
                    elif actual_first_player_present:
                        if number_full_fields == 1:
                            value += value_big_single_me
                        elif number_full_fields == 2:
                            value += value_big_double_me
                    elif actual_second_player_present:
                        if number_full_fields == 1:
                            value -= value_big_single_opponent
                        elif number_full_fields == 2:
                            value -= value_big_double_opponent
                            # izracunaj vrednosti pozicije v mrezi malih polj
            if not is_field_finished[big_field]:
                for small_field in [0, 1, 2, 3, 6]:
                    if small_field == 0:
                        for row_containing_small_field in [rows[0], rows[3], rows[6]]:
                            number_full_fields = 0
                            actual_first_player_present = False
                            actual_second_player_present = False
                            for small_field_in_row in row_containing_small_field:
                                if game_board[big_field][small_field_in_row] == actual_first_player:
                                    actual_first_player_present = True
                                    number_full_fields += 1
                                elif game_board[big_field][small_field_in_row] is not None:
                                    actual_second_player_present = True
                                    number_full_fields += 1
                            if actual_first_player_present and actual_second_player_present:
                                pass
                            elif actual_first_player_present:
                                if number_full_fields == 1:
                                    value += value_small_single_me
                                elif number_full_fields == 2:
                                    value += value_small_double_me
                            elif actual_second_player_present:
                                if number_full_fields == 1:
                                    value -= value_small_single_opponent
                                elif number_full_fields == 2:
                                    value -= value_small_double_opponent

                    elif small_field == 1:
                        for row_containing_small_field in [rows[4]]:
                            number_full_fields = 0
                            actual_first_player_present = False
                            actual_second_player_present = False
                            for small_field_in_row in row_containing_small_field:
                                if game_board[big_field][small_field_in_row] == actual_first_player:
                                    actual_first_player_present = True
                                    number_full_fields += 1
                                elif game_board[big_field][small_field_in_row] is not None:
                                    actual_second_player_present = True
                                    number_full_fields += 1
                            if actual_first_player_present and actual_second_player_present:
                                pass
                            elif actual_first_player_present:
                                if number_full_fields == 1:
                                    value += value_small_single_me
                                elif number_full_fields == 2:
                                    value += value_small_double_me
                            elif actual_second_player_present:
                                if number_full_fields == 1:
                                    value -= value_small_single_opponent
                                elif number_full_fields == 2:
                                    value -= value_small_double_opponent

                    elif small_field == 2:
                        for row_containing_small_field in [rows[5], rows[7]]:
                            number_full_fields = 0
                            actual_first_player_present = False
                            actual_second_player_present = False
                            for small_field_in_row in row_containing_small_field:
                                if game_board[big_field][small_field_in_row] == actual_first_player:
                                    actual_first_player_present = True
                                    number_full_fields += 1
                                elif game_board[big_field][small_field_in_row] is not None:
                                    actual_second_player_present = True
                                    number_full_fields += 1
                            if actual_first_player_present and actual_second_player_present:
                                pass
                            elif actual_first_player_present:
                                if number_full_fields == 1:
                                    value += value_small_single_me
                                elif number_full_fields == 2:
                                    value += value_small_double_me
                            elif actual_second_player_present:
                                if number_full_fields == 1:
                                    value -= value_small_single_opponent
                                elif number_full_fields == 2:
                                    value -= value_small_double_opponent

                    elif small_field == 3:
                        for row_containing_small_field in [rows[1]]:
                            number_full_fields = 0
                            actual_first_player_present = False
                            actual_second_player_present = False
                            for small_field_in_row in row_containing_small_field:
                                if game_board[big_field][small_field_in_row] == actual_first_player:
                                    actual_first_player_present = True
                                    number_full_fields += 1
                                elif game_board[big_field][small_field_in_row] is not None:
                                    actual_second_player_present = True
                                    number_full_fields += 1
                            if actual_first_player_present and actual_second_player_present:
                                pass
                            elif actual_first_player_present:
                                if number_full_fields == 1:
                                    value += value_small_single_me
                                elif number_full_fields == 2:
                                    value += value_small_double_me
                            elif actual_second_player_present:
                                if number_full_fields == 1:
                                    value -= value_small_single_opponent
                                elif number_full_fields == 2:
                                    value -= value_small_double_opponent

                    elif small_field == 6:
                        for row_containing_small_field in [rows[2]]:
                            number_full_fields = 0
                            actual_first_player_present = False
                            actual_second_player_present = False
                            for small_field_in_row in row_containing_small_field:
                                if game_board[big_field][small_field_in_row] == actual_first_player:
                                    actual_first_player_present = True
                                    number_full_fields += 1
                                elif game_board[big_field][small_field_in_row] is not None:
                                    actual_second_player_present = True
                                    number_full_fields += 1
                            if actual_first_player_present and actual_second_player_present:
                                pass
                            elif actual_first_player_present:
                                if number_full_fields == 1:
                                    value += value_small_single_me
                                elif number_full_fields == 2:
                                    value += value_small_double_me
                            elif actual_second_player_present:
                                if number_full_fields == 1:
                                    value -= value_small_single_opponent
                                elif number_full_fields == 2:
                                    value -= value_small_double_opponent
    return value  # + randint(1, 15)


class UltimateTicTacToe(object):
    def __init__(self):
        # igralna plosca je predstavljena kot seznam devetih seznamov dolzine 9, vsak izmed slednjih predstavlja en
        # nonant (big_field).
        # is_field_finished pove, kateri nonanti so ze zakljuceni. winners_big_fields pove, kdo so zmagovalci
        # zakljucenih nonantov.
        # available_fields je seznam nonantov, v katere je mozno igrati naslednjo potezo

        self.game_board = [[None for _ in range(9)] for _ in range(9)]
        self.winner = None
        self.is_field_finished = [False for _ in range(9)]  # odvec (enum)
        self.winners_big_fields = [None for _ in range(9)]
        self.first_player = True
        self.available_fields = [True for _ in range(9)]
        self.previously_available_fields = None
        # 3 vrste, 3 stolpci, 2 diagonali za tri v vrsto:
        self.rows = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)]
        # vizualna predstavitev igralne plosce. Za debugging, lazjo vizualizacijo etc. Glej metodi board_representation
        # in big_field_representation.
        self.visual_game_board = None
        self.visual_big_fields = None

    # izvede zeljeno potezo in izracuna vse, kar je potrebno.
    def move(self, big_field_coordinate, small_field_coordinate):

        [self.game_board, self.available_fields, self.is_field_finished, self.winners_big_fields, self.first_player,
         self.previously_available_fields] = move(self.game_board, self.available_fields, self.first_player,
                                                  self.is_field_finished, self.winners_big_fields,
                                                  big_field_coordinate, small_field_coordinate)

    # razveljavi zadnjo potezo.
    def undo_move(self, big_field_coordinate, small_field_coordinate):

        [self.game_board, self.is_field_finished, self.winners_big_fields, self.previously_available_fields,
         self.first_player] = undo_last_move(self.game_board, self.first_player, self.is_field_finished,
                                             self.winners_big_fields, big_field_coordinate, small_field_coordinate,
                                             self.previously_available_fields)

    def game_over(self):

        [winner, over] = game_over(self.is_field_finished, self.winners_big_fields, self.available_fields)
        self.winner = winner
        return over

    # vizualna predstavitev igralne plosce:
    # 1. mreza nonantov:
    def big_fields_representation(self):

        self.visual_big_fields = [None for _ in range(9)]
        for field in range(9):
            if self.winners_big_fields[field] is None:
                self.visual_big_fields[field] = ' '
            elif self.winners_big_fields[field]:
                self.visual_big_fields[field] = 'X'
            else:
                self.visual_big_fields[field] = 'O'
        print('---------------' + '\n'
              + str(self.visual_big_fields[0:3]) + '\n'
              + str(self.visual_big_fields[3:6]) + '\n'
              + str(self.visual_big_fields[6:9]) + '\n'
              + '---------------')

    # 2. celotna igralna plosca
    def board_representation(self):

        self.visual_game_board = [[None for _ in range(9)] for _ in range(9)]
        for k in range(9):
            for j in range(9):
                if self.game_board[k][j] is None:
                    self.visual_game_board[k][j] = ' '
                elif not self.game_board[k][j]:
                    self.visual_game_board[k][j] = 'O'
                else:
                    self.visual_game_board[k][j] = 'X'
        print('---------------------------------------------' + '\n'
              + str(self.visual_game_board[0][0:3]) + str(self.visual_game_board[1][0:3]) + str(
              self.visual_game_board[2][0:3]) + '\n'
              + str(self.visual_game_board[0][3:6]) + str(self.visual_game_board[1][3:6]) + str(
              self.visual_game_board[2][3:6]) + '\n'
              + str(self.visual_game_board[0][6:9]) + str(self.visual_game_board[1][6:9]) + str(
              self.visual_game_board[2][6:9]) + '\n'
              + '---------------------------------------------' + '\n'
              + str(self.visual_game_board[3][0:3]) + str(self.visual_game_board[4][0:3]) + str(
              self.visual_game_board[5][0:3]) + '\n'
              + str(self.visual_game_board[3][3:6]) + str(self.visual_game_board[4][3:6]) + str(
              self.visual_game_board[5][3:6]) + '\n'
              + str(self.visual_game_board[3][6:9]) + str(self.visual_game_board[4][6:9]) + str(
              self.visual_game_board[5][6:9]) + '\n'
              + '---------------------------------------------' + '\n'
              + str(self.visual_game_board[6][0:3]) + str(self.visual_game_board[7][0:3]) + str(
              self.visual_game_board[8][0:3]) + '\n'
              + str(self.visual_game_board[6][3:6]) + str(self.visual_game_board[7][3:6]) + str(
              self.visual_game_board[8][3:6]) + '\n'
              + str(self.visual_game_board[6][6:9]) + str(self.visual_game_board[7][6:9]) + str(
              self.visual_game_board[8][6:9]) + '\n'
              + '---------------------------------------------')


def play(first_player_parameters=(10, 10, 20, 20, 100, 100, 200, 200, 30, 30),
         second_player_parameters=(10, 10, 20, 20, 100, 100, 200, 200, 30, 30),
         max_depth1=6,
         max_depth2=6,
         no_tries=1,
         human_first_player=False,
         human_second_player=False,
         save_results=False,
         results_comment=None,
         show_game=False,
         min_time=0,
         max_time = 10):
    given_depth1 = max_depth1
    given_depth2 = max_depth2
    [first, tie, second] = [0, 0, 0]
    if human_first_player or human_second_player:
        show_game = True
    for i in range(no_tries):
        game = UltimateTicTacToe()
        if show_game:
            game.board_representation()
            move_count = 0
        while not game.game_over():
            available_fields_list = []
            for field in range(9):
                if game.available_fields[field]:
                    available_fields_list.append(field + 1)
            if show_game:
                print('Current legal big fields are: ', available_fields_list)
            if game.first_player:
                if human_first_player:
                    chosen_move = human_player_move(game.first_player,
                                                    game.game_board,
                                                    game.available_fields)
                else:
                    start = time()
                    chosen_move = alpha_beta_pruning(max_depth1,
                                                     max_depth1,
                                                     float('-inf'),
                                                     float('inf'),
                                                     True,
                                                     game.first_player,
                                                     game.game_board,
                                                     game.first_player,
                                                     game.available_fields,
                                                     game.is_field_finished,
                                                     game.winners_big_fields,
                                                     first_player_parameters[0],
                                                     first_player_parameters[1],
                                                     first_player_parameters[2],
                                                     first_player_parameters[3],
                                                     first_player_parameters[4],
                                                     first_player_parameters[5],
                                                     first_player_parameters[6],
                                                     first_player_parameters[7],
                                                     first_player_parameters[8],
                                                     first_player_parameters[9])
                    finish = time()
                    current_time = finish - start
                    if move_count > 30:
                        if current_time < 5:
                            sleep(5 - current_time)
                    if current_time < min_time:
                        max_depth1 += 1
                        print("povecana globina")
                    elif current_time > max_time and max_depth1 > given_depth1:
                        max_depth1 -= 1
                        print("zmanjsana globina")
                    if move_count == 30:
                        input('.')

            else:
                if human_second_player:
                    chosen_move = human_player_move(game.first_player,
                                                    game.game_board,
                                                    game.available_fields)
                else:
                    start = time()
                    chosen_move = alpha_beta_pruning(max_depth2,
                                                     max_depth2,
                                                     float('-inf'),
                                                     float('inf'),
                                                     True,
                                                     game.first_player,
                                                     game.game_board,
                                                     game.first_player,
                                                     game.available_fields,
                                                     game.is_field_finished,
                                                     game.winners_big_fields,
                                                     second_player_parameters[0],
                                                     second_player_parameters[1],
                                                     second_player_parameters[2],
                                                     second_player_parameters[3],
                                                     second_player_parameters[4],
                                                     second_player_parameters[5],
                                                     second_player_parameters[6],
                                                     second_player_parameters[7],
                                                     second_player_parameters[8],
                                                     second_player_parameters[9])
                    finish = time()
                    current_time = finish - start
                    if move_count > 30:
                        if current_time < 5:
                            sleep(5 - current_time)

                    if current_time < min_time:
                        max_depth2 += 1
                        print("povecana globina")
                    elif current_time > max_time and max_depth2 > given_depth2:
                        max_depth2 -= 1
                        print("zmanjsana globina")
                    if move_count == 30:
                        input('.')
            if show_game:
                print('Last move: ', chosen_move[1][0] + 1, chosen_move[1][1] + 1)
            game.move(chosen_move[1][0], chosen_move[1][1])
            if show_game:
                game.board_representation()
                game.big_fields_representation()
                move_count += 1
                print('move_count = ', move_count)
        if game.winner:
            first += 1
        elif game.winner is None:
            tie += 1
        else:
            second += 1
        print("game no. = ", i + 1, "; winner is: ", ['X' if game.winner else 'O'], "; results so far: ", first, tie, second)
    results = [first, tie, second]
    if save_results:
        with open('results.txt', 'a') as results_file:
            results_file.write(str(datetime.now()) + '\n' + 'no. of games: ' + str(no_tries) + '\n'
                               + 'depth: ' + str(max_depth1) + ', ' + str(max_depth2) + '\n'
                               + str(results) + '\n')
            if results_comment:
                results_file.write('Comment: ' + str(results_comment) + '\n' + '\n')
            else:
                results_file.write('\n')
    return results


def play_tournament(first_player_parameters=(10, 10, 20, 20, 100, 100, 200, 200, 30, 30),
                    second_player_parameters=(10, 10, 20, 20, 100, 100, 200, 200, 30, 30),
                    max_depth1=6,
                    max_depth2=6,
                    no_tries=10):
    [first, tie, second] = [0, 0, 0]
    for i in range(no_tries):
        game = UltimateTicTacToe()
        while not game.game_over():
            if game.first_player:
                chosen_move = alpha_beta_pruning(max_depth1,
                                                 max_depth1,
                                                 float('-inf'),
                                                 float('inf'),
                                                 True,
                                                 game.first_player,
                                                 game.game_board,
                                                 game.first_player,
                                                 game.available_fields,
                                                 game.is_field_finished,
                                                 game.winners_big_fields,
                                                 first_player_parameters[0],
                                                 first_player_parameters[1],
                                                 first_player_parameters[2],
                                                 first_player_parameters[3],
                                                 first_player_parameters[4],
                                                 first_player_parameters[5],
                                                 first_player_parameters[6],
                                                 first_player_parameters[7],
                                                 first_player_parameters[8],
                                                 first_player_parameters[9])
            else:
                chosen_move = alpha_beta_pruning(max_depth2,
                                                 max_depth2,
                                                 float('-inf'),
                                                 float('inf'),
                                                 True,
                                                 game.first_player,
                                                 game.game_board,
                                                 game.first_player,
                                                 game.available_fields,
                                                 game.is_field_finished,
                                                 game.winners_big_fields,
                                                 second_player_parameters[0],
                                                 second_player_parameters[1],
                                                 second_player_parameters[2],
                                                 second_player_parameters[3],
                                                 second_player_parameters[4],
                                                 second_player_parameters[5],
                                                 second_player_parameters[6],
                                                 second_player_parameters[7],
                                                 second_player_parameters[8],
                                                 second_player_parameters[9])
            game.move(chosen_move[1][0], chosen_move[1][1])
        if game.winner:
            first += 1
        elif game.winner is None:
            tie += 1
        else:
            second += 1
    results = [first, tie, second]
    return results


# primer igre:
game = play(first_player_parameters=[0, 12, 205, 448, 0, 1220, 5697, 5274, 350, -344],
               max_depth1=7,
               max_depth2=7,
               no_tries=1,
               human_first_player=False,
               human_second_player=False,
               save_results=False,
               results_comment='666. iteracija vs. zacetek',
               show_game=True,
               min_time=0,
               max_time=15)

# class PlayGUI():
#     def __init__(self, master):
#         #
#         # zacetni parametri: barve, sirina crt
#         #
#         self.velikost = master.winfo_screenheight() * (2/3)  # velikost igralnega polja v pikslih
#         self.velikostPolja = self.velikost // 9  # velikost polj
#         self.velikostKvadranta = self.velikost // 3  # velikost kvadranta
#         self.odmik = max(self.velikostPolja // 15, 1)  # odmik malih x, o, od roba polja
#         self.odmik2 = max(self.velikostKvadranta // 15, 10)  # odmik velikih X, O, od roba kvadranta
#         self.barvaTankih = 'black'  # barva crt, ki razmejujejo polja
#         self.sirinaTankih = 1  # sirina crt, ki razmejujejo polja
#         self.barvaKrepkih = 'green'  # barva crt, ki razmejujejo kvadrante
#         self.sirinaKrepkih = min(self.velikost // 150 + 1, 4)  # sirina crt, ki razmejujejo kvadrante
#         self.barvaPoudarka = 'grey85'  # barva ozadja poudarjenega kvadranta
#         self.barvaVrste = 'gray'
#         self.barvaRemija = 'gray'
#         # izdelamo menu:
#         #
#         menu = Menu(master)
#         master.config(menu=menu) # Dodamo menu
#
#         igra_menu = Menu(menu)
#         menu.add_cascade(label="Igra", menu=igra_menu)
#         igra_menu.add_command(label="X=Človek, O=Človek",         command=lambda: self.nova_igra(True, True))
#         igra_menu.add_command(label="X=Človek, O=Računalnik",     command=lambda: self.nova_igra(True, False))
#         igra_menu.add_command(label="X=Računalnik, O=Človek",     command=lambda: self.nova_igra(False, True))
#         igra_menu.add_command(label="X=Računalnik, O=Računalnik", command=lambda: self.nova_igra(False, False))
#
#         menu.add_command(label="Izhod", command=master.destroy)
#
#         #
#         # zacetno risanje in tekst:
#         #
#         self.na_potezi = StringVar(master, value='X')  # igro vedno zacne igralec X
#         self.polje = Canvas(master, width=self.velikost, height=self.velikost)  # izris igralnega polja
#         self.polje.grid(row=1, column=0, columnspan=2)
#         self.polje.bind('<Button-1>', self.klik)  # klik je pritisk na levi miskin gumb
#
#         Label(text='Na potezi je',
#               font = ('Helvetica', 18)).grid(row=0, column=0)  # napis na vrhu okna
#         Label(textvariable=self.na_potezi,
#               font = ('Helvetica', 18)).grid(row=0, column=1)  # izpise igralca, ki je na potezi
#         self.polje.delete(ALL)  # pobrisemo vse narisane objekte na igralnem polju
#         for i in range(3):
#             for j in range(3):
#                 self.polje.create_rectangle(i * self.velikostKvadranta, j * self.velikostKvadranta,
#                                             (i + 1) * self.velikostKvadranta, (j + 1) * self.velikostKvadranta,
#                                             fill=self.polje['background'], outline='',
#                                             tags='nonant{0}'.format(str(3 * i + j + 1)))
#         # Tanke razmejitvene crte
#         for i in [1, 2, 4, 5, 7, 8]:
#             self.polje.create_line(i * self.velikost // 9, 0,
#                                    i * self.velikost // 9, self.velikost,
#                                    fill=self.barvaTankih,
#                                    width=self.sirinaTankih)
#             self.polje.create_line(0, i * self.velikost // 9,
#                                    self.velikost, i * self.velikost // 9,
#                                    fill=self.barvaTankih,
#                                    width=self.sirinaTankih)
#         # Krepke razmejitvene crte
#         for i in [3, 6]:
#             self.polje.create_line(i * self.velikost // 9, 0,
#                                    i * self.velikost // 9, self.velikost,
#                                    fill=self.barvaKrepkih,
#                                    width=self.sirinaKrepkih)
#             self.polje.create_line(0, i * self.velikost // 9,
#                                    self.velikost, i * self.velikost // 9,
#                                    fill=self.barvaKrepkih,
#                                    width=self.sirinaKrepkih)
#     def klik(self):
#         i = int(floor(event.x / self.velikostPolja))
#         j = int(floor(event.y / self.velikostPolja))
#         return [i, j]
#
#     def nova_igra(self,
#                   human_first_player,
#                   human_second_player,
#                   first_player_parameters=(0, 12, 205, 448, 0, 1220, 5697, 5274, 350, -344),
#                   second_player_parameters=(10, 10, 20, 20, 100, 100, 200, 200, 30, 30),
#                   max_depth1=7,
#                   max_depth2=7):
#         game = UltimateTicTacToe()
#         if game.first_player:
#             if human_first_player:
#                 [i, j] = self.klik()
#                 ostanek_i = i % 3
#                 ostanek_j = j % 3
#                 veliki_i = i // 3
#                 veliki_j = j // 3
#                 koordinata_kvadranta = 3 * veliki_i + veliki_j
#                 koordinata_polja = 3 * ostanek_i + ostanek_j
#                 if game.available_fields[koordinata_kvadranta]:
#                     if game.game_board[koordinata_kvadranta][koordinata_polja] is None:
#                         game.move(koordinata_kvadranta, koordinata_polja)
#                         self.narisi_mali_x(i, j)
#
#
#             else:
#                 chosen_move = alpha_beta_pruning(max_depth1,
#                                                  max_depth1,
#                                                  float('-inf'),
#                                                  float('inf'),
#                                                  True,
#                                                  game.first_player,
#                                                  game.game_board,
#                                                  game.first_player,
#                                                  game.available_fields,
#                                                  game.is_field_finished,
#                                                  game.winners_big_fields,
#                                                  first_player_parameters[0],
#                                                  first_player_parameters[1],
#                                                  first_player_parameters[2],
#                                                  first_player_parameters[3],
#                                                  first_player_parameters[4],
#                                                  first_player_parameters[5],
#                                                  first_player_parameters[6],
#                                                  first_player_parameters[7],
#                                                  first_player_parameters[8],
#                                                  first_player_parameters[9])
#
#         else:
#             if human_second_player:
#                 chosen_move = human_player_move(game.first_player,
#                                                 game.game_board,
#                                                 game.available_fields)
#             else:
#                 chosen_move = alpha_beta_pruning(max_depth2,
#                                                  max_depth2,
#                                                  float('-inf'),
#                                                  float('inf'),
#                                                  True,
#                                                  game.first_player,
#                                                  game.game_board,
#                                                  game.first_player,
#                                                  game.available_fields,
#                                                  game.is_field_finished,
#                                                  game.winners_big_fields,
#                                                  second_player_parameters[0],
#                                                  second_player_parameters[1],
#                                                  second_player_parameters[2],
#                                                  second_player_parameters[3],
#                                                  second_player_parameters[4],
#                                                  second_player_parameters[5],
#                                                  second_player_parameters[6],
#                                                  second_player_parameters[7],
#                                                  second_player_parameters[8],
#                                                  second_player_parameters[9])
#         game.move(chosen_move[1][0], chosen_move[1][1])
#     #
#     # Funkcije za risanje potrebnih geometrijskih objektov
#     #
#     def narisi_mali_x(self, i, j):
#         x = i * self.velikostPolja
#         y = j * self.velikostPolja
#         self.polje.create_line(x + self.odmik, y + self.odmik,
#                                x + self.velikostPolja - self.odmik, y + self.velikostPolja - self.odmik)
#         self.polje.create_line(x + self.velikostPolja - self.odmik, y + self.odmik,
#                                x + self.odmik, y + self.velikostPolja - self.odmik)
#
#     def narisi_mali_o(self, i, j):
#         x = i * self.velikostPolja
#         y = j * self.velikostPolja
#         self.polje.create_oval(x + self.odmik, y + self.odmik,
#                                x + self.velikostPolja - self.odmik, y + self.velikostPolja - self.odmik)
#
#     def narisi_velik_x(self, i, j):
#         x = i * self.velikostKvadranta
#         y = j * self.velikostKvadranta
#         self.polje.create_line(x + self.odmik2, y + self.odmik2,
#                                x + self.velikostKvadranta - self.odmik2, y + self.velikostKvadranta - self.odmik2,
#                                fill=self.barvaKrepkih,
#                                width=self.sirinaKrepkih)
#         self.polje.create_line(x + self.velikostKvadranta - self.odmik2, y + self.odmik2,
#                                x + self.odmik2, y + self.velikostKvadranta - self.odmik2,
#                                fill=self.barvaKrepkih,
#                                width=self.sirinaKrepkih)
#
#     def narisi_velik_o(self, i, j):
#         x = i * self.velikostKvadranta
#         y = j * self.velikostKvadranta
#         self.polje.create_oval(x + self.odmik2, y + self.odmik2,
#                                x + self.velikostKvadranta - self.odmik2, y + self.velikostKvadranta - self.odmik2,
#                                fill='',
#                                outline=self.barvaKrepkih,
#                                width=self.sirinaKrepkih)
#
#     # Funkcija klik, ki doloca, kaj se zgodi ob kliku na igralno povrsino
#
# root = Tk()
#
# root.title('Ultimate Tic-Tac-Toe')
#
# aplikacija = PlayGUI(root)
#
# root.mainloop()