__author__ = 'Jure Bevc'

from random import random, shuffle, sample, randint, gauss, lognormvariate
from itertools import combinations
from ultimateTicTacToe import play_tournament
from time import time
import os


def create_descendants(ancestors):
    genome_length = len(ancestors[0])
    no_ancestors = len(ancestors)
    pairs = list(combinations(ancestors, 2))
    descendants = [None for _ in range(no_ancestors ** 2 - no_ancestors)]
    descendants = ancestors + descendants
    index = len(ancestors)
    for pair in pairs:
        fathers_genome = pair[0]
        mothers_genome = pair[1]
        child1 = [None for _ in range(genome_length)]
        child2 = [None for _ in range(genome_length)]
        genes_from_father = list(sample(range(genome_length), genome_length // 2))
        for gene in range(genome_length):
            if gene in genes_from_father:
                child1[gene] = fathers_genome[gene]
            else:
                child1[gene] = mothers_genome[gene]
            random_bit = randint(0, 2)
            if random_bit == 0:
                child2[gene] = fathers_genome[gene]
            elif random_bit == 1:
                child2[gene] = (fathers_genome[gene] + mothers_genome[gene]) / 2
            else:
                child2[gene] = mothers_genome[gene]
        descendants[index] = child1
        descendants[index + 1] = child2
        index += 2
    return descendants


def create_mutations(original_pool, mutation_rate=0.1):
    genome_length = len(original_pool[0])
    num_individuals = len(original_pool)
    mutated_pool = [[None for _ in range(genome_length)] for _ in range(num_individuals)]
    for individual in range(num_individuals):
        for gene in range(genome_length):
            mutation = 0
            chance = random()
            if chance < mutation_rate:
                mutation = gauss(0, 1)
                if 1 > mutation > -1:
                    mutation *= gauss(0, 1)
                    mutation *= gauss(0, 1)
                    mutation *= gauss(0, 1)
                else:
                    mutation *= gauss(0, 1)
                    if 1 > mutation > -1:
                        mutation *= lognormvariate(0, 2)
            mutated_pool[individual][gene] = (1 + mutation) * original_pool[individual][gene]
    return mutated_pool


def tournament(participants, no_of_matches=10, depth=6, group_size=4, show_results=False):
    pairings = list(combinations(range(group_size), 2))
    no_of_survivors = len(participants)
    shuffle(participants)
    while no_of_survivors > 4:
        survivors = []
        if show_results:
            print('------------------------------' + '\n' + 'ROUND OF ', no_of_survivors, '\n' +
                  '------------------------------')
        for group_start_index in range(0, no_of_survivors, group_size):
            group_results = [0 for _ in range(group_size)]
            if show_results:
                if group_start_index != 0:
                    print('Next week, some distance away ...')

            for pairing in pairings:
                if show_results:
                    print('\n' + 'RIVALRY OF THE DAY: ' + '\n', two_survivors[0], '\n'
                          + 'vs.' +
                          '\n', participants[group_start_index + pairing[1]])
                first_leg_1 = play_tournament(first_player_parameters=participants[group_start_index + pairing[0]],
                                              second_player_parameters=participants[group_start_index + pairing[1]],
                                              max_depth1=depth,
                                              max_depth2=depth,
                                              no_tries=no_of_matches // 2)
                second_leg_1 = play_tournament(first_player_parameters=participants[group_start_index + pairing[1]],
                                               second_player_parameters=participants[group_start_index + pairing[0]],
                                               max_depth1=depth,
                                               max_depth2=depth,
                                               no_tries=no_of_matches // 2)
                points_first_player = 3 * first_leg_1[0] + 1 * first_leg_1[1] +\
                    1 * second_leg_1[1] + 3 * second_leg_1[2]
                points_second_player = 3 * first_leg_1[2] + 1 * first_leg_1[1] +\
                    1 * second_leg_1[1] + 3 * second_leg_1[0]
                # sicer eden od zadnjih dveh parametrov nikjer ne pride v postev:
                first_leg_2 = play_tournament(first_player_parameters=participants[group_start_index + pairing[0]],
                                              second_player_parameters=participants[group_start_index + pairing[1]],
                                              max_depth1=depth - 1,
                                              max_depth2=depth - 1,
                                              no_tries=no_of_matches // 2)
                second_leg_2 = play_tournament(first_player_parameters=participants[group_start_index + pairing[1]],
                                               second_player_parameters=participants[group_start_index + pairing[0]],
                                               max_depth1=depth - 1,
                                               max_depth2=depth - 1,
                                               no_tries=no_of_matches // 2)
                points_first_player += 3 * first_leg_2[0] + 1 * first_leg_2[1] + \
                    1 * second_leg_2[1] + 3 * second_leg_2[2]
                points_second_player += 3 * first_leg_2[2] + 1 * first_leg_2[1] + \
                    1 * second_leg_2[1] + 3 * second_leg_2[0]
                group_results[pairing[0]] += points_first_player
                group_results[pairing[1]] += points_second_player
                if show_results:
                    print('----------' + '\n'
                          + 'Night falls. Results: ' + '\n' + 'First creature: ', points_first_player,
                          ' points,' + '\n' + 'Second creature: ', points_second_player, ' points.' + '\n' +
                          '--------------------')
            group_results_representation = group_results[:]
            group_winner_index = group_results.index(max(group_results))
            group_results[group_winner_index] = 0
            group_runner_up_index = group_results.index(max(group_results))
            group_winner = participants[group_start_index + group_winner_index]
            group_runner_up = participants[group_start_index + group_runner_up_index]
            survivors.append(group_winner)
            survivors.append(group_runner_up)
            if show_results:
                print('\n' + '\n' + 'WEEKLY SUMMARY:' + '\n',
                      group_results_representation[0], 'points: creature ', participants[group_start_index + 0], '\n',
                      group_results_representation[1], 'points: creature ', participants[group_start_index + 1], '\n',
                      group_results_representation[2], 'points: creature ', participants[group_start_index + 2], '\n',
                      group_results_representation[3], 'points: creature ', participants[group_start_index + 3], '\n'
                      + '------------------------------------------------------------' + '\n')
        no_of_survivors = len(survivors)
        participants = survivors
        if show_results:
            print(survivors)
            print(str(no_of_survivors) + '/' + '32')
    if show_results:
        print('End of an epoch.')
        print('These creatures survived:' + '\n', survivors)
    return survivors


def iterate_tournament(no_iterations=False,  # ce podamo nek int, se pac izvede tolikokrat. Sicer se izvaja v infty.
                       no_tournament_matches=2,
                       match_depth=6,
                       tournament_group_size=4,
                       show_results=False):
    results_directory = "genetic_results"
    if not os.path.exists(results_directory):
        os.makedirs(results_directory)
    search_last_iteration = 1
    current_directory = os.path.dirname(__file__)
    results_directory = "genetic_results"
    while True:
        file_name = "{0}.txt".format(search_last_iteration)
        absolute_path = os.path.join(current_directory, results_directory, file_name)
        if os.path.isfile(absolute_path):
            search_last_iteration += 1
        else:
            if search_last_iteration == 1:
                if show_results:
                    print('No life forms detected ... LET THERE BE LIFE!')
                else:
                    print('Running evolution.py. The program runs until it is interrupted. Results are saved with '
                          + 'each iteration in /genetic_results/n.txt, where n is the number of' +
                          'the current iteration.')

                participants = create_starting_participants()
                if show_results:
                    print(' ... And lo, there was life.')
                break
            else:
                file_name = '{0}.txt'.format(search_last_iteration - 1)
                previous_iteration_results_path = os.path.join(current_directory, results_directory, file_name)
                if show_results:
                    print('Existing life forms detected: ', file_name, ' in: ', previous_iteration_results_path)
                    print('--------------------------------------------------------------------------------')
                else:
                    print('Running evolution.py. The program runs until it is interrupted. Results are saved with '
                          + 'every iteration in /genetic_results/n.txt, where n is the number of' +
                          ' the current iteration.')
                with open(previous_iteration_results_path) as previous_iteration_results:
                    previous_iteration_survivors = []
                    for line in previous_iteration_results:
                        numbers_str = line.split(',')
                        numbers_float = [float(x) for x in numbers_str]
                        previous_iteration_survivors.append(numbers_float)
                new_family = create_descendants(previous_iteration_survivors)
                if search_last_iteration % 4 == 0:
                    participants = new_family + inject_randomness()
                else:
                    new_mutations = create_mutations(new_family)
                    participants = new_family + new_mutations
                break
    survivors = None
    if bool(no_iterations):
        iteration = 0
        while iteration < no_iterations:
            file_name = "{0}.txt".format(search_last_iteration)
            absolute_path = os.path.join(current_directory, results_directory, file_name)
            survivors = tournament(participants,
                                   no_of_matches=no_tournament_matches,
                                   depth=match_depth,
                                   group_size=tournament_group_size,
                                   show_results=show_results)
            with open(absolute_path, 'a') as evolution_results_file:
                evolution_results_file.write('\n'.join([','.join([str(feature) for feature in survivor])
                                                        for survivor in survivors]))
            iteration += 1
            search_last_iteration += 1
            descendants = create_descendants(survivors)
            if search_last_iteration % 8 == 0:
                participants = descendants + inject_randomness()
            else:
                mutants = create_mutations(descendants, 0.1)
                participants = descendants + mutants
        return survivors
    else:
        while True:
            file_name = "{0}.txt".format(search_last_iteration)
            absolute_path = os.path.join(current_directory, results_directory, file_name)
            survivors = tournament(participants,
                                   no_of_matches=no_tournament_matches,
                                   depth=match_depth,
                                   group_size=tournament_group_size,
                                   show_results=show_results)
            with open(absolute_path, 'a') as evolution_results_file:
                evolution_results_file.write('\n'.join([','.join([str(feature) for feature in survivor])
                                                        for survivor in survivors]))
            search_last_iteration += 1
            descendants = create_descendants(survivors)
            if search_last_iteration % 4 == 0:
                participants = descendants + inject_randomness()
            else:
                mutants = create_mutations(descendants, 0.1)
                participants = descendants + mutants


def create_starting_participants(mut_rate=0.1, create_starting_mutants=True):
    starting_participants = [[10, 10, 20, 20, 100, 100, 200, 200, 30, 30],
                             [15, 5, 30, 10, 150, 50, 300, 100, 45, 15],
                             [5, 15, 10, 30, 50, 150, 100, 300, 15, 45],
                             [10, 10, 21, 21, 100, 100, 201, 201, 30, 30]]
    starting_family = create_descendants(starting_participants)
    if create_starting_mutants:
        starting_mutants = create_mutations(starting_family, mutation_rate=mut_rate)
        return starting_family + starting_mutants
    else:
        return starting_family


def inject_randomness():
    random_participants = [[random() for _ in range(10)] for _ in range(16)]
    return random_participants

# iterate_tournament(match_depth=5, show_results=True)


def select_true_winners():
    for iteration_number in [501]:
        print(iteration_number)
        current_directory = os.path.dirname(__file__)
        reading_folder = 'genetic_results_klemen'
        writing_folder = 'singleton_survivors'
        file_name = "{0}.txt".format(iteration_number)
        absolute_read_path = os.path.join(current_directory, reading_folder, file_name)
        results = [0, 0]
        with open(absolute_read_path) as survivors:
            all_survivors = []
            for line in survivors:
                numbers_str = line.split(',')
                numbers_float = [float(x) for x in numbers_str]
                all_survivors.append(numbers_float)
        two_survivors = [all_survivors[i] for i in (0,2)]
        first_leg_1 = play_tournament(first_player_parameters=two_survivors[0],
                                      second_player_parameters=two_survivors[1],
                                      max_depth1=5,
                                      max_depth2=5,
                                      no_tries=1)
        second_leg_1 = play_tournament(first_player_parameters=two_survivors[1],
                                       second_player_parameters=two_survivors[0],
                                       max_depth1=5,
                                       max_depth2=5,
                                       no_tries=1)
        points_first_player = 3 * first_leg_1[0] + 1 * first_leg_1[1] +\
            1 * second_leg_1[1] + 3 * second_leg_1[2]
        points_second_player = 3 * first_leg_1[2] + 1 * first_leg_1[1] +\
            1 * second_leg_1[1] + 3 * second_leg_1[0]
        # sicer eden od zadnjih dveh parametrov nikjer ne pride v postev:
        first_leg_2 = play_tournament(first_player_parameters=two_survivors[0],
                                      second_player_parameters=two_survivors[1],
                                      max_depth1=4,
                                      max_depth2=4,
                                      no_tries=1)
        second_leg_2 = play_tournament(first_player_parameters=two_survivors[1],
                                       second_player_parameters=two_survivors[0],
                                       max_depth1=4,
                                       max_depth2=4,
                                       no_tries=1)
        points_first_player += 3 * first_leg_2[0] + 1 * first_leg_2[1] + \
            1 * second_leg_2[1] + 3 * second_leg_2[2]
        points_second_player += 3 * first_leg_2[2] + 1 * first_leg_2[1] + \
            1 * second_leg_2[1] + 3 * second_leg_2[0]
        results[0] += points_first_player
        results[1] += points_second_player
        if results[0] >= results[1]:
            winner = two_survivors[0]
        else:
            winner = two_survivors[1]
        for ind in range(len(winner)):
            winner[ind] = round(winner[ind])
        absolute_write_path = os.path.join(current_directory, writing_folder, file_name)
        print(absolute_write_path)
        with open(absolute_write_path, 'a') as evolution_results_file:
            evolution_results_file.write(','.join([str(feature) for feature in winner]))

select_true_winners()